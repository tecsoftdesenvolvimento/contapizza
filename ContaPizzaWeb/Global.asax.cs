﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ContaPizzaWeb.Controllers;
using ContaPizzaWeb.Models;

namespace ContaPizzaWeb
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
            Database.SetInitializer<UsersContext>(new Initializer());
        }

        public class Initializer : System.Data.Entity.CreateDatabaseIfNotExists<UsersContext>
        {
            protected override void Seed(UsersContext context)
            {   
                context.MotivosModels.Add(new MotivoModels() { Motivo = "Atrasos" });
                context.SaveChanges();
            }
        }
        /*
        public class Initializer : DropCreateDatabaseAlways<UsersContext>
        {
            protected override void Seed(UsersContext context)
            {
                if (context.MotivosModels.Count() == 0)
                {
                    context.MotivosModels.Add(new MotivoModels() {Motivo = "Atrasos"});
                    context.SaveChanges();
                }
            }
        }
         * */
    }
}