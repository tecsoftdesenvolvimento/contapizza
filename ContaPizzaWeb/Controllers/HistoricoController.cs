﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ContaPizzaWeb.Models;

namespace ContaPizzaWeb.Controllers
{
    public class HistoricoController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /Historico/

        public ActionResult Index()
        {
            var pizzasmodels = db.PizzasModels.Include(p => p.Func).Include(p => p.Mot);
            return View(pizzasmodels.Where(p => p.Status.Equals(("Pago"))).ToList());
        }

        //
        // GET: /Historico/Details/5

        public ActionResult Details(int id = 0)
        {
            PizzaModels pizzamodels = db.PizzasModels.Find(id);
            if (pizzamodels == null)
            {
                return HttpNotFound();
            }
            return View(pizzamodels);
        }

        //
        // GET: /Historico/Create

        public ActionResult Create()
        {
            ViewBag.IdFunc = new SelectList(db.FuncionariosModels, "IdFunc", "Funcionario");
            ViewBag.IdMot = new SelectList(db.MotivosModels, "IdMot", "Motivo");
            return View();
        }

        //
        // POST: /Historico/Create

        [HttpPost]
        public ActionResult Create(PizzaModels pizzamodels)
        {
            if (ModelState.IsValid)
            {
                db.PizzasModels.Add(pizzamodels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdFunc = new SelectList(db.FuncionariosModels, "IdFunc", "Funcionario", pizzamodels.IdFunc);
            ViewBag.IdMot = new SelectList(db.MotivosModels, "IdMot", "Motivo", pizzamodels.IdMot);
            return View(pizzamodels);
        }

        //
        // GET: /Historico/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PizzaModels pizzamodels = db.PizzasModels.Find(id);
            if (pizzamodels == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdFunc = new SelectList(db.FuncionariosModels, "IdFunc", "Funcionario", pizzamodels.IdFunc);
            ViewBag.IdMot = new SelectList(db.MotivosModels, "IdMot", "Motivo", pizzamodels.IdMot);
            return View(pizzamodels);
        }

        //
        // POST: /Historico/Edit/5

        [HttpPost]
        public ActionResult Edit(PizzaModels pizzamodels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pizzamodels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdFunc = new SelectList(db.FuncionariosModels, "IdFunc", "Funcionario", pizzamodels.IdFunc);
            ViewBag.IdMot = new SelectList(db.MotivosModels, "IdMot", "Motivo", pizzamodels.IdMot);
            return View(pizzamodels);
        }

        //
        // GET: /Historico/Delete/5

        public ActionResult Delete(int id = 0)
        {
            PizzaModels pizzamodels = db.PizzasModels.Find(id);
            if (pizzamodels == null)
            {
                return HttpNotFound();
            }
            return View(pizzamodels);
        }

        //
        // POST: /Historico/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            PizzaModels pizzamodels = db.PizzasModels.Find(id);
            db.PizzasModels.Remove(pizzamodels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}