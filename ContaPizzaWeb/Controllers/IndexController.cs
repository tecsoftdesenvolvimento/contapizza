﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ContaPizzaWeb.Models;

namespace ContaPizzaWeb.Controllers
{
    public class IndexController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /Index/

        public ActionResult Index()
        {
            var pizzasmodels = db.PizzasModels.Include(p => p.Func).Include(p => p.Mot);
            return View(pizzasmodels.Where(p => p.Status.Equals(("Pend"))).OrderBy(p => p.Func.Funcionario).ToList());
        }

        [HttpGet]
        public JsonResult PegarPizzaFuncionarios()
        {
            var pizzasmodels = db.PizzasModels.Where(a => a.Status=="Pend").Include(p => p.Func).Include(p => p.Mot);
            var pizzasAgrupadas = pizzasmodels.GroupBy(a => a.Func.Funcionario).Select(a => new { Nome = a.Key, NumeroPizzas = a.Count() }).OrderByDescending(p => p.NumeroPizzas).ToList();
            return Json(pizzasAgrupadas, "Json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult PegarListaDevedores()
        {
            var pizzasmodels = db.PizzasModels.Include(p => p.Func).Include(p => p.Mot);
            var listaPizza = pizzasmodels.Where(p => p.Status.Equals(("Pend"))).OrderBy(p => p.Func.Funcionario).ToList();

            return Json(listaPizza, "Json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Index/Details/5

        public ActionResult Details(int id = 0)
        {
            PizzaModels pizzamodels = db.PizzasModels.Find(id);
            if (pizzamodels == null)
            {
                return HttpNotFound();
            }
            return View(pizzamodels);
        }

        //
        // GET: /Index/Create

        public ActionResult Create()
        {
            ViewBag.IdFunc = new SelectList(db.FuncionariosModels, "IdFunc", "Funcionario");
            ViewBag.IdMot = new SelectList(db.MotivosModels, "IdMot", "Motivo");
            return View();
        }

        //
        // POST: /Index/Create

        [HttpPost]
        public ActionResult Create(PizzaModels pizzamodels)
        {
            if (ModelState.IsValid)
            {
                db.PizzasModels.Add(pizzamodels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdFunc = new SelectList(db.FuncionariosModels, "IdFunc", "Funcionario", pizzamodels.IdFunc);
            ViewBag.IdMot = new SelectList(db.MotivosModels, "IdMot", "Motivo", pizzamodels.IdMot);
            return View(pizzamodels);
        }

        //
        // GET: /Index/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PizzaModels pizzamodels = db.PizzasModels.Find(id);
            if (pizzamodels == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdFunc = new SelectList(db.FuncionariosModels, "IdFunc", "Funcionario", pizzamodels.IdFunc);
            ViewBag.IdMot = new SelectList(db.MotivosModels, "IdMot", "Motivo", pizzamodels.IdMot);
            return View(pizzamodels);
        }

        //
        // POST: /Index/Edit/5

        [HttpPost]
        public ActionResult Edit(PizzaModels pizzamodels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pizzamodels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdFunc = new SelectList(db.FuncionariosModels, "IdFunc", "Funcionario", pizzamodels.IdFunc);
            ViewBag.IdMot = new SelectList(db.MotivosModels, "IdMot", "Motivo", pizzamodels.IdMot);
            return View(pizzamodels);
        }

        //
        // GET: /Index/Delete/5

        public ActionResult Delete(int id = 0)
        {
            PizzaModels pizzamodels = db.PizzasModels.Find(id);
            if (pizzamodels == null)
            {
                return HttpNotFound();
            }
            return View(pizzamodels);
        }

        //
        // POST: /Index/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            PizzaModels pizzamodels = db.PizzasModels.Find(id);
            db.PizzasModels.Remove(pizzamodels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}