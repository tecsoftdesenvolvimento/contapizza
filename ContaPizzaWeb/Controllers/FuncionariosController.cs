﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ContaPizzaWeb.Models;

namespace ContaPizzaWeb.Controllers
{
    public class FuncionariosController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /Funcionarios/

        public ActionResult Index()
        {
            return View(db.FuncionariosModels.ToList());
        }

        //
        // GET: /Funcionarios/Details/5

        public ActionResult Details(int id = 0)
        {
            FuncionarioModels funcionariomodels = db.FuncionariosModels.Find(id);
            if (funcionariomodels == null)
            {
                return HttpNotFound();
            }
            return View(funcionariomodels);
        }

        //
        // GET: /Funcionarios/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Funcionarios/Create

        [HttpPost]
        public ActionResult Create(FuncionarioModels funcionariomodels)
        {
            if (ModelState.IsValid)
            {
                db.FuncionariosModels.Add(funcionariomodels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(funcionariomodels);
        }

        //
        // GET: /Funcionarios/Edit/5

        public ActionResult Edit(int id = 0)
        {
            FuncionarioModels funcionariomodels = db.FuncionariosModels.Find(id);
            if (funcionariomodels == null)
            {
                return HttpNotFound();
            }
            return View(funcionariomodels);
        }

        //
        // POST: /Funcionarios/Edit/5

        [HttpPost]
        public ActionResult Edit(FuncionarioModels funcionariomodels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(funcionariomodels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(funcionariomodels);
        }

        //
        // GET: /Funcionarios/Delete/5

        public ActionResult Delete(int id = 0)
        {
            FuncionarioModels funcionariomodels = db.FuncionariosModels.Find(id);
            if (funcionariomodels == null)
            {
                return HttpNotFound();
            }
            return View(funcionariomodels);
        }

        //
        // POST: /Funcionarios/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            FuncionarioModels funcionariomodels = db.FuncionariosModels.Find(id);
            db.FuncionariosModels.Remove(funcionariomodels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}