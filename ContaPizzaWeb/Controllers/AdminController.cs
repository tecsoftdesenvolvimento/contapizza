﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ContaPizzaWeb.Models;

namespace ContaPizzaWeb.Controllers
{
    public class AdminController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /Admin/

        public ActionResult Index()
        {
            var pizzasmodels = db.PizzasModels.Include(p => p.Func).Include(p => p.Mot);
            return View(pizzasmodels.Where(p => p.Status.Equals(("Pend"))).OrderBy(p => p.Func.Funcionario).ToList());
        }

        //
        // GET: /Admin/Details/5

        public ActionResult Details(int id = 0)
        {
            PizzaModels pizzamodels = db.PizzasModels.Find(id);
            if (pizzamodels == null)
            {
                return HttpNotFound();
            }
            return View(pizzamodels);
        }

        //
        // GET: /Admin/Create

        public ActionResult Create()
        {
            ViewBag.IdFunc = new SelectList(db.FuncionariosModels, "IdFunc", "Funcionario");
            ViewBag.IdMot = new SelectList(db.MotivosModels, "IdMot", "Motivo");
            ViewBag.DataAtual = DateTime.Today;
            return View();
        }

        //
        // POST: /Admin/Create

        [HttpPost]
        public ActionResult Create(PizzaModels pizzamodels)
        {
            if (ModelState.IsValid)
            {
                db.PizzasModels.Add(pizzamodels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdFunc = new SelectList(db.FuncionariosModels, "IdFunc", "Funcionario", pizzamodels.IdFunc);
            ViewBag.IdMot = new SelectList(db.MotivosModels, "IdMot", "Motivo", pizzamodels.IdMot);
            return View(pizzamodels);
        }

        //
        // GET: /Admin/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PizzaModels pizzamodels = db.PizzasModels.Find(id);
            if (pizzamodels == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdFunc = new SelectList(db.FuncionariosModels, "IdFunc", "Funcionario", pizzamodels.IdFunc);
            ViewBag.IdMot = new SelectList(db.MotivosModels, "IdMot", "Motivo", pizzamodels.IdMot);
            return View(pizzamodels);
        }

        //
        // POST: /Admin/Edit/5

        [HttpPost]
        public ActionResult Edit(PizzaModels pizzamodels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pizzamodels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdFunc = new SelectList(db.FuncionariosModels, "IdFunc", "Funcionario", pizzamodels.IdFunc);
            ViewBag.IdMot = new SelectList(db.MotivosModels, "IdMot", "Motivo", pizzamodels.IdMot);
            return View(pizzamodels);
        }

        //
        // GET: /Admin/Delete/5

        public ActionResult Delete(int id = 0)
        {
            PizzaModels pizzamodels = db.PizzasModels.Find(id);
            if (pizzamodels == null)
            {
                return HttpNotFound();
            }
            return View(pizzamodels);
        }


        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            PizzaModels pizzamodels = db.PizzasModels.Find(id);
            db.PizzasModels.Remove(pizzamodels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult DeleteReg(int id = 0)
        {
            PizzaModels pizzamodels = db.PizzasModels.Find(id);
            if (pizzamodels == null)
            {
                return HttpNotFound();
            }
            return View(pizzamodels);
        }
        

        [HttpPost, ActionName("DeleteReg")]
        public ActionResult DeleteRegConfirmed(int id)
        {
            PizzaModels pizzamodels = db.PizzasModels.Find(id);
            pizzamodels.Status = "Pago";
            pizzamodels.DataPag = DateTime.Today.ToString("dd/MM/yyyy");
            db.Entry(pizzamodels).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public JsonResult AtualizaDetalhes(int IdPizza, string Detalhes)
        {
            try
            {
                PizzaModels pizzamodels = db.PizzasModels.Find(IdPizza);
                pizzamodels.Detalhes = Detalhes;
                db.Entry(pizzamodels).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { Status = 200, Mensagem = "Sucesso" });
            }
            catch (Exception ex)
            {
                return Json(new { Status = 500, Mensagem = ex.Message });
            }
        }
       

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}