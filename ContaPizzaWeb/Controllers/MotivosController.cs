﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ContaPizzaWeb.Models;

namespace ContaPizzaWeb.Controllers
{
    public class MotivosController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /Motivos/

        public ActionResult Index()
        {
            return View(db.MotivosModels.ToList());
        }

        //
        // GET: /Motivos/Details/5

        public ActionResult Details(int id = 0)
        {
            MotivoModels motivomodels = db.MotivosModels.Find(id);
            if (motivomodels == null)
            {
                return HttpNotFound();
            }
            return View(motivomodels);
        }

        //
        // GET: /Motivos/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Motivos/Create

        [HttpPost]
        public ActionResult Create(MotivoModels motivomodels)
        {
            if (ModelState.IsValid)
            {
                db.MotivosModels.Add(motivomodels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(motivomodels);
        }

        //
        // GET: /Motivos/Edit/5

        public ActionResult Edit(int id = 0)
        {
            MotivoModels motivomodels = db.MotivosModels.Find(id);
            if (motivomodels == null)
            {
                return HttpNotFound();
            }
            return View(motivomodels);
        }

        //
        // POST: /Motivos/Edit/5

        [HttpPost]
        public ActionResult Edit(MotivoModels motivomodels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(motivomodels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(motivomodels);
        }

        //
        // GET: /Motivos/Delete/5

        public ActionResult Delete(int id = 0)
        {
            MotivoModels motivomodels = db.MotivosModels.Find(id);
            if (motivomodels == null)
            {
                return HttpNotFound();
            }
            return View(motivomodels);
        }

        //
        // POST: /Motivos/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            MotivoModels motivomodels = db.MotivosModels.Find(id);
            db.MotivosModels.Remove(motivomodels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}