﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using ContaPizzaWeb.Models;
using ContaPizzaWeb.Models.Repositorios;

namespace ContaPizzaWeb.Controllers
{
    public class AtrasosController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /Atrasos/

        public ActionResult Index()
        {
            var atrasosmodels = db.AtrasosModels.Include(a => a.Func);
            return View(atrasosmodels.OrderBy(f => f.Func.Funcionario).ToList());
        }

        [HttpGet]
        public JsonResult PegarAtrasosFuncionarios()
        {
            var atrasosmodels = db.AtrasosModels.Include(a => a.Func);
            var atrasosAgrupados = atrasosmodels.GroupBy(a => a.Func.Funcionario).Select(a => new { Nome = a.Key, NumeroAtrasos = a.Count() }).OrderBy(p => p.Nome).ToList();
            return Json(atrasosAgrupados, "Json", System.Text.Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Atrasos/Details/5

        public ActionResult Details(int id = 0)
        {
            AtrasosModels atrasosmodels = db.AtrasosModels.Find(id);
            if (atrasosmodels == null)
            {
                return HttpNotFound();
            }
            return View(atrasosmodels);
        }

        //
        // GET: /Atrasos/Create

        public ActionResult Create()
        {
            ViewBag.IdFunc = new SelectList(db.FuncionariosModels, "IdFunc", "Funcionario");
            return View();
        }

        //
        // POST: /Atrasos/Create

        [HttpPost]
        public ActionResult Create(AtrasosModels atrasosmodels)
        {
            if (ModelState.IsValid)
            {
                var verificaAtrasos = db.AtrasosModels.Where(p => p.IdFunc.Equals(atrasosmodels.IdFunc));
                if (verificaAtrasos.Count() >= 3)
                {
                    var dias = "Atrasos: ";
                    foreach (var atraso in verificaAtrasos)
                    {
                        dias += atraso.DataAtraso.ToString("dd/MM") + " - ";
                    }
                    dias += atrasosmodels.DataAtraso.ToString("dd/MM");
                    PizzaModels contaPizza = new PizzaModels();
                    contaPizza.Detalhes = dias;
                    contaPizza.IdFunc = atrasosmodels.IdFunc;
                    contaPizza.IdMot = 1;
                    contaPizza.RegData = DateTime.Now;
                    db.PizzasModels.Add(contaPizza);
                    foreach (var atraso in verificaAtrasos)
                    {
                        db.Set<AtrasosModels>().Remove(atraso);
                    }
                    db.SaveChanges();
                    return RedirectToAction("Index");
                    }
                    else
                    {
                        db.AtrasosModels.Add(atrasosmodels);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }    
                    //db.AtrasosModels.ToList().Where(x=> x.IdFunc == atrasosmodels.IdFunc).Select(x=> x).ToList();
            }

            ViewBag.IdFunc = new SelectList(db.FuncionariosModels, "IdFunc", "Funcionario", atrasosmodels.IdFunc);
            return View(atrasosmodels);
        }

        //
        // GET: /Atrasos/Edit/5

        public ActionResult Edit(int id = 0)
        {
            AtrasosModels atrasosmodels = db.AtrasosModels.Find(id);
            if (atrasosmodels == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdFunc = new SelectList(db.FuncionariosModels, "IdFunc", "Funcionario", atrasosmodels.IdFunc);
            return View(atrasosmodels);
        }

        //
        // POST: /Atrasos/Edit/5

        [HttpPost]
        public ActionResult Edit(AtrasosModels atrasosmodels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(atrasosmodels).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdFunc = new SelectList(db.FuncionariosModels, "IdFunc", "Funcionario", atrasosmodels.IdFunc);
            return View(atrasosmodels);
        }

        //
        // GET: /Atrasos/Delete/5

        public ActionResult Delete(int id = 0)
        {
            AtrasosModels atrasosmodels = db.AtrasosModels.Find(id);
            if (atrasosmodels == null)
            {
                return HttpNotFound();
            }
            return View(atrasosmodels);
        }

        //
        // POST: /Atrasos/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            AtrasosModels atrasosmodels = db.AtrasosModels.Find(id);
            db.AtrasosModels.Remove(atrasosmodels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public string DeleteConfirmedAll()
        {
            var atrasos = db.AtrasosModels.Where(p => p.IdAtraso != null);
            foreach (var atraso in atrasos)
            {
                db.AtrasosModels.Remove(atraso);
            }
            try
            {
                db.SaveChanges();
                return "Sucesso";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}