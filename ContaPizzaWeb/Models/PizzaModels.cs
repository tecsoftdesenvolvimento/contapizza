﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Microsoft.Ajax.Utilities;

namespace ContaPizzaWeb.Models
{
    [Table("Funcionarios")]
    public class FuncionarioModels  
    {
        [Key]
        public int IdFunc { get; set; }
        [Required(ErrorMessage = "Digite o nome do funcionário.")]
        [Display(Name = "Funcionário")]
        public string Funcionario { get; set; }
    }

    [Table("Motivos")]
    public class MotivoModels
    {
        [Key]
        public int IdMot { get; set; }
        [Required(ErrorMessage = "Digite um motivo.")]
        [Display(Name = "Motivo")]
        public string Motivo { get; set; }
    }

    [Table("Pizzas")]
    public class PizzaModels
    {
        [Key]
        public int IdPizza { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(Name = "Data")]
        public DateTime RegData { get; set; }
        [Display(Name = "Funcionário")]
        public int IdFunc { get; set; }
        [Display(Name = "Motivo")]
        public int IdMot { get; set; }
        public string Status { get; set; }
        [Display(Name = "Data do Pagamento")]
        public String DataPag { get; set; }
        public string Detalhes { get; set; }
        
        public virtual FuncionarioModels Func { get; set; }
        public virtual MotivoModels Mot { get; set; }

        public PizzaModels()
        {
            Status = "Pend";
            Detalhes = null;
        }  
    }
    [Table("Atrasos")]
    public class AtrasosModels
    {
        [Key]
        public int IdAtraso { get; set; }
        [Required(ErrorMessage = "Selecione um funcionário.")]
        public int IdFunc { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        [Display(Name = "Dia do Atraso")]
        public DateTime DataAtraso { get; set; }

        public virtual FuncionarioModels Func { get; set; }

        public AtrasosModels()
        {
            DataAtraso = DateTime.Today;
        }
    } 
}