﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ContaPizzaWeb.Models.Repositorios
{
    public class Repositorio: IDisposable
    {
        protected readonly DbContext contexto;
          public Repositorio(DbContext contexto) 
          {
                    this.contexto = contexto;
          }

        public Repositorio()
        {
            this.contexto = new UsersContext();
        }


          public virtual void Adicionar<T>(T item)where T:class 
          {
                    contexto.Set<T>().Add(item);
                    contexto.SaveChanges();
          }
          public virtual void Remover<T>(T item)where T:class 
          {
                    contexto.Set<T>().Remove(item);
                    contexto.SaveChanges();
          }
          public virtual void Editar<T>(T item)where T:class 
          {
                    contexto.Entry(item).State = EntityState.Modified;
                    contexto.SaveChanges();
          }
          public virtual T ObtemPorId<T>(object id)where T:class 
          {
                    return contexto.Set<T>().Find(id);
          }
          public virtual IQueryable<T> Tudo<T>()where T:class 
          {
                    return contexto.Set<T>();
          }
          public void Dispose()
          {
                    contexto.Dispose();
          }
    }
}